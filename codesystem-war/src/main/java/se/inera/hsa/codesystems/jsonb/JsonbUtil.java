package se.inera.hsa.codesystems.jsonb;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

public class JsonbUtil {

    private static Jsonb jsonb = null;

    public static Jsonb jsonb() {
        if (jsonb == null) {
            jsonb = JsonbBuilder.create();
        }
        return jsonb;
    }
}
