package se.inera.hsa.codesystems.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.openapi.annotations.Operation;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import se.inera.hsa.codesystems.model.Code;
import se.inera.hsa.codesystems.model.CodeSystem;
import se.inera.hsa.codesystems.service.ExpireDateAwareCodeSystemProvider;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Tag(name = "CodeSystem")
@RequestScoped
@Path("codesystems")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CodeSystemResource {
    private static final Logger log = LogManager.getLogger(CodeSystemResource.class);

    @Inject
    private ExpireDateAwareCodeSystemProvider service;

    @Operation(
            summary = "Get all code systems",
            description = "Returns all stored code systems"
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "A list of available code systems (name and OID required fields and always returne for each code system)",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    type = SchemaType.ARRAY,
                                    implementation = CodeSystem.class,
                                    description = "CodeSystem list"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            )
    })
    @GET
    public List<CodeSystem> getCodeSystems() {
        log.info("GET - all codes systems");
        return service.getCodeSystems();
    }

    @Operation(
            summary = "Get a specific code system",
            description = "Returns a complete code system given an id (OID)"
    )
    @Parameter(
            name = "codeSystemId",
            required = true,
            description = "OID"
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "Returns information about a specific code system (name and OID required fields and always returned)",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = CodeSystem.class,
                                    description = "Code system"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No code system found for id (OID)"
            )
    })
    @GET
    @Path("{codeSystemId}")
    public CodeSystem getCodeSystem(@PathParam("codeSystemId") String oid) {
        return service.getCodesystem(oid);
    }

    @Operation(
            summary = "Get codes for a specific code system",
            description = "Returns all code objects for a given code system id (OID). Code, name and codeDescription required fields and always returned."
    )
    @Parameter(
            name = "codeSystemId",
            required = true,
            description = "OID"
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "Returns code information given a specific code system (code, name and codeDescription required fields and always returned)",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    type = SchemaType.ARRAY,
                                    implementation = Code.class,
                                    description = "Code"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No codes found for id (OID)"
            )
    })
    @GET
    @Path("{codeSystemId}/codes")
    public List<Code> getCodes(@PathParam("codeSystemId") String codeSystemId) {
        log.debug("Metod getCodes");
        return service.getCodes(codeSystemId);
    }

    @Operation(
            summary = "Get code object for a specific code in a specific code system",
            description = "Returns a code object for a given code in a specific code system. Code, name and codeDescription required fields and always returned."
    )
    @Parameter(
            name = "codeSystemId",
            required = true,
            description = "OID"
    )
    @Parameter(
            name = "codeId",
            required = true,
            description = "code"
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "Returns a code object for a given code in a specific code system (code, name and codeDescription required fields and always returned)",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = Code.class,
                                    description = "Code"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No code found for given code system and code"
            )
    })
    @GET
    @Path("{codeSystemId}/codes/{codeId}")
    public Code getCode(@PathParam("codeSystemId") String codeSystemId, @PathParam("codeId") String codeId) {
        return service.getCode(codeSystemId, codeId);
    }

    @Operation(
            summary = "Delete a complete code system",
            description = "Delete a complete code system given an id (OID)"
    )
    @Parameter(
            name = "codeSystemId",
            required = true,
            description = "OID"
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "202",
                    description = "Delete request accepted"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No code system found for id (OID)"
            )
    })
    @DELETE
    @Path("{codeSystemId}/codes/{code}")
    public Response deleteCode(@PathParam("codeSystemId") String oid, @PathParam("code") String code) {
        service.deleteCode(oid, code);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete a complete code system",
            description = "Delete a complete code system given an id (OID)"
    )
    @Parameter(
            name = "codeSystemId",
            required = true,
            description = "OID"
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "202",
                    description = "Delete request accepted"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No code system found for id (OID)"
            )
    })
    @DELETE
    @Path("{codeSystemId}")
    public Response deleteCodeSystem(@PathParam("codeSystemId") String oid) {
        service.deleteCodesystem(oid);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Add/update a codesystem.",
            description = "Add or update code codesystem, "
    )
    @RequestBody(
            name = "CodeSystem",
            required = true,
            content = @Content(
                    schema = @Schema(
                            implementation = CodeSystem.class,
                            description = "CodeSystem object"
                    ),
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Add or update request successful, location header returned"
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Bad request, invalid CodeSystem object"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No code system found for id (OID)"
            )
    })
    @PUT
    public Response putCodeSystem(@Valid CodeSystem codeSystem) {
        service.persistCodesystem(codeSystem);
        Response response = Response.status(Response.Status.NO_CONTENT).build();
        response.getHeaders().add("Location", UriBuilder.fromPath(codeSystem.codeSystemId).build());
        return response;
    }

    @Operation(
            summary = "Add/update a code object. Ex. date-format yyyy-mm-ddThh:MM:mmmmmmZ",
            description = "Add or update code object under a given codesystem"
    )
    @Parameter(
            name = "codeSystemId",
            required = true,
            description = "OID"
    )
    @RequestBody(
            name = "Code",
            required = true,
            content = @Content(
                    schema = @Schema(
                            implementation = Code.class,
                            description = "Code object"
                    ),
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Add or update request successful, location header returned"
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Bad request, invalid Code object"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "No code system found for id (OID)"
            )
    })
    @PUT
    @Path("{codeSystemId}/codes")
    public Response putCode(@PathParam("codeSystemId") String codeSystemId, @Valid Code code) {
        service.persistCode(codeSystemId, code);
        Response response = Response.status(Response.Status.NO_CONTENT).build();
        response.getHeaders().add("Location", UriBuilder.fromPath(codeSystemId).path("codes").path(code.code).build());
        return response;
    }
}
