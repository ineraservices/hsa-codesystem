package se.inera.hsa.codesystems.service;

import se.inera.hsa.codesystems.model.Code;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * This class removes old expired codes
 */
@ApplicationScoped
public class ExpireDateAwareCodeSystemProvider extends SqlServerCodeSystemProvider {

    /**
     *
     * @param codeSystemOid codesystem oid
     * @throws NotFoundException if codesystem not found
     */
    @Override
    public List<Code> getCodes(String codeSystemOid) throws NotFoundException {
        ZonedDateTime now = ZonedDateTime.now();
        List<Code> codeList = super.getCodes(codeSystemOid);
        List<Code> codesToRemove = new ArrayList<>();
        for(Code code: codeList) {
            if (code.expireDate != null && code.expireDate.isBefore(now)) {
                codesToRemove.add(code);
            }
        }
        codeList.removeAll(codesToRemove);
        return codeList;
    }

    /**
     *
     * @param codeSystemOid codesystem oid
     * @param codeId code id
     * @throws NotFoundException if code has expired
     */
    @Override
    public Code getCode(String codeSystemOid, String codeId) throws NotFoundException {
        ZonedDateTime now = ZonedDateTime.now();
        Code code = super.getCode(codeSystemOid, codeId);

        if (code.expireDate != null && code.expireDate.isBefore(now)) {
            throw new NotFoundException("Code: " + code.code + " has expired: " + code.expireDate);
        }
        return code;
    }
}
