package se.inera.hsa.codesystems.service;

import org.jdbi.v3.core.Jdbi;
import se.inera.hsa.codesystems.jsonb.JsonbUtil;
import se.inera.hsa.codesystems.model.Code;
import se.inera.hsa.codesystems.model.CodeSystem;
import se.inera.hsa.codesystems.model.mapper.CodeRowMapper;
import se.inera.hsa.codesystems.model.mapper.CodeSystemRowMapper;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.json.bind.JsonbException;
import javax.sql.DataSource;
import javax.ws.rs.NotFoundException;
import java.util.List;

/**
 * This class implements the CodesystemProvider interface using sqlserver
 *
 * uses
 *  - jndi-named datasource "sqlServerDataSource", it is configured in the openliberty server.xml
 *      in project "hsa-docker/docker/hsa-openliberty-rest/server.xml"
 *  - jdbi, Jdbi is built on top of JDBC. If your database has a JDBC driver, you can use Jdbi
 *    with it. Jdbi improves JDBC’s rough interface, providing a more natural Java database interface
 *    that is easy to bind to your domain data types.
 */
@ApplicationScoped
public class SqlServerCodeSystemProvider implements CodesystemProvider {
    @Resource(name="sqlServerDataSource")
    private DataSource codesystemDataSource;

    private Jdbi jdbi;

    @PostConstruct
    public void start() {
        try {
            System.out.println("SqlServerCodeSystemProvider: creating instance....");
            jdbi = Jdbi.create(codesystemDataSource);
        } catch(Exception ex) {
            System.out.println("SqlServerCodeSystemProvider: Can't instantiate SqlServerCodeSystemProvider");
            ex.printStackTrace();
        }
    }

    @Override
    public void persistCodesystem(CodeSystem codeSystem) {
        if (isCodeSystemExists(codeSystem.codeSystemId)) {
            updateCodesystem(codeSystem);
        } else {
            saveCodesystem(codeSystem);
        }
    }

    private void saveCodesystem(CodeSystem codeSystem) {
        String codesystemJson = JsonbUtil.jsonb().toJson(codeSystem);
        jdbi.withHandle(handle -> {
            String sql = "insert into codesystem (codesystem_oid, codesystem_json) VALUES (:codeSystemId, :codesystemJson)";
            return handle.createUpdate(sql)
                    .bind("codeSystemId", codeSystem.codeSystemId)
                    .bind("codesystemJson", codesystemJson)
                    .execute();
        });
    }

    private void updateCodesystem(CodeSystem codeSystem) {
        String codesystemJson = JsonbUtil.jsonb().toJson(codeSystem);
        jdbi.withHandle(handle -> {
            String sql = "update codesystem set codesystem_json = :codesystemJson where codesystem_oid = :codeSystemId";
            return handle.createUpdate(sql)
                    .bind("codesystemJson", codesystemJson)
                    .bind("codeSystemId", codeSystem.codeSystemId)
                    .execute();
        });
    }

    @Override
    public void persistCode(String oid, Code code) {
        if (isCodeExists(oid, code.code)) {
            updateCode(oid, code);
        } else {
            saveCode(oid, code);
        }

    }

    private void saveCode(String parent_oid, Code code) {
        String codeJson = JsonbUtil.jsonb().toJson(code);
        jdbi.withHandle(handle -> {
            String sql = "insert into code (code_id, parent_oid, code_json) VALUES (:codeId, :parent_oid, :codeJson)";
            return handle.createUpdate(sql)
                    .bind("codeId", code.code)
                    .bind("parent_oid", parent_oid)
                    .bind("codeJson", codeJson)
                    .execute();
        });
    }

    private void updateCode(String parent_oid, Code code) {
        String codeJson = JsonbUtil.jsonb().toJson(code);
        jdbi.withHandle(handle -> {
            String sql = "update code set code_json = :codeJson where parent_oid = :parent_oid and code_id = :codeId";
            return handle.createUpdate(sql)
                    .bind("codeJson", codeJson)
                    .bind("parent_oid", parent_oid)
                    .bind("codeId", code.code)
                    .execute();
        });
    }

    @Override
    public List<CodeSystem> getCodeSystems() {
        String sql = "select codesystem_oid, codesystem_json from codesystem;";
        return jdbi.withHandle(handle -> handle.createQuery(sql)
                .map(new CodeSystemRowMapper())
                .list());
    }

    /**
     *
     * @param codeSystemOid codesystem to get
     * @return codesystem
     * @throws NotFoundException if nothing is found
     */
    @Override
    public CodeSystem getCodesystem(String codeSystemOid) throws NotFoundException{
        String sql = "select codesystem_oid, codesystem_json from codesystem where codesystem_oid = :codeSystemOid";
        return jdbi.withHandle(handle -> handle.createQuery(sql)
                .bind("codeSystemOid", codeSystemOid)
                .map(new CodeSystemRowMapper())
                .findOne())
                .orElseThrow(() -> new NotFoundException(
                        "No codesystem with codesystem id: " + codeSystemOid + " found."));
    }

    /**
     *
     * @param codeSystemOid codesystem to get codes from
     * @return list of codes
     * @throws NotFoundException if the codesystem is not found
     */
    @Override
    public List<Code> getCodes(String codeSystemOid) throws NotFoundException{
        getCodesystem(codeSystemOid);
        String sql = "select code_id, code_json from code where parent_oid = :codeSystemOid";
        return jdbi.withHandle(handle -> handle.createQuery(sql)
                .bind("codeSystemOid", codeSystemOid)
                .map(new CodeRowMapper())
                .list());
    }

    /**
     *
     * @param codeSystemOid codesystem oid
     * @param codeId code id
     * @return code
     * @throws NotFoundException if code is not found
     */
    @Override
    public Code getCode(String codeSystemOid, String codeId)  throws NotFoundException{
        String sql = "select code_id, code_json from code where parent_oid = :codeSystemOid and code_id = :codeId";
        return jdbi.withHandle(handle -> handle.createQuery(sql)
                .bind("codeSystemOid", codeSystemOid)
                .bind("codeId", codeId)
                .map(new CodeRowMapper())
                .findOne()
                .orElseThrow(() -> new NotFoundException(
                        "No code with codesystem id: " + codeSystemOid + " and code id:" + codeId + "found.")));
    }

    /**
     * maybe not the best implementation...
     */
    private boolean isCodeExists(String codeSystemOid, String codeId) {
        try {
            getCode(codeSystemOid, codeId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * maybe not the best implementation...
     */
    private boolean isCodeSystemExists(String codeSystemOid) {
        try {
            getCodesystem(codeSystemOid);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @param codeSystemOid codesystem oid
     * @throws NotFoundException id codesystem is not found (before delete)
     */
    @Override
    public void deleteCodesystem(String codeSystemOid) throws NotFoundException {
        getCodesystem(codeSystemOid);
        jdbi.withHandle(handle -> {
            String sql = "delete from codesystem where codesystem_oid = :codeSystemId;";
            return handle.createUpdate(sql)
                    .bind("codeSystemId", codeSystemOid)
                    .execute();
        });
    }

    /**
     *
     * @param codeSystemOid codesystem oid
     * @param codeSystemOid code id
     * @throws NotFoundException id code is not found (before delete)
     */
    @Override
    public void deleteCode(String codeSystemOid, String codeId) throws NotFoundException {
        getCode(codeSystemOid, codeId);
        jdbi.withHandle(handle -> {
            String sql = "delete from code where parent_oid = :codeSystemId and code_id = :codeId;";
            return handle.createUpdate(sql)
                    .bind("codeSystemId", codeSystemOid)
                    .bind("codeId", codeId)
                    .execute();
        });
    }
}