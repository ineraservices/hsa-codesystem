package se.inera.hsa.codesystems.model;

import javax.json.bind.JsonbBuilder;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.ZonedDateTime;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

public class Code implements Serializable {

    @Schema(required = true)
    @NotBlank
    public String code;

    @Schema(required = true)
    @NotBlank
    public String name;

    @Schema(required = true)
    @NotBlank
    public String parentOid;

    public String codeDescription;

    public ZonedDateTime lastModifiedDate = ZonedDateTime.now();

    @Schema(
            description = "yyyy-MM-ddTHH:mm:ss.SSSXX"
    )
    public ZonedDateTime expireDate;

    public String extra1;

    public String extra2;

    public String extra3;

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
