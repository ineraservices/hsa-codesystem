package se.inera.hsa.codesystems.model.mapper;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import se.inera.hsa.codesystems.jsonb.JsonbUtil;
import se.inera.hsa.codesystems.model.Code;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeRowMapper implements RowMapper<Code> {
    public CodeRowMapper() {
    }

    @Override
    public Code map(ResultSet rs, StatementContext ctx) throws SQLException {
        String codeJson = rs.getNString("code_json");
        return JsonbUtil.jsonb().fromJson(codeJson, Code.class);
    }
}
