package se.inera.hsa.codesystems;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.servers.Server;
import org.eclipse.microprofile.openapi.annotations.servers.ServerVariable;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@OpenAPIDefinition(
        info = @Info(
                version = "0.1",
                title = "HSA 2.0 Code System API",
                description = "This api provides CRUD functionality for " +
                        "the Code System microservice wich is part of the bundle of components and services constituting<br/>" +
                        "the HSA 2.0 eco system. The Code System API is a support function providing verification and information.",
                license = @License(
                        name = "Apache License Version 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html"
                )
        ),
        servers = {
                @Server(url = "http://{CODE_SYSTEM_URL}:{CODE_SYSTEM_PORT}/codesystem",
                        variables = {
                                @ServerVariable(
                                        name = "CODE_SYSTEM_URL",
                                        defaultValue = "localhost"
                                ),
                                @ServerVariable(
                                        name = "CODE_SYSTEM_PORT",
                                        defaultValue = "9081"
                                )
                        }),
                @Server(url = "https://{CODE_SYSTEM_URL}:{CODE_SYSTEM_PORT}/codesystem",
                        variables = {
                                @ServerVariable(
                                        name = "CODE_SYSTEM_URL",
                                        defaultValue = "hsa-codesystem-dhsa.app-test1.ind-ocp.sth.basefarm.net"
                                ),
                                @ServerVariable(
                                        name = "CODE_SYSTEM_PORT",
                                        defaultValue = "443"
                                )
                        })
        }
)
@ApplicationPath("v1")
public class JAXRSConfiguration extends Application {
}
