package se.inera.hsa.codesystems.service;

import se.inera.hsa.codesystems.model.Code;
import se.inera.hsa.codesystems.model.CodeSystem;

import java.util.List;

public interface CodesystemProvider {
    void persistCodesystem(CodeSystem codeSystem);

    void persistCode(String codeSystemOid, Code code);

    List<CodeSystem> getCodeSystems();

    CodeSystem getCodesystem(String codeSystemOid);

    List<Code> getCodes(String codeSystemOid);

    Code getCode(String codeSystemOid, String codeId);

    void deleteCodesystem(String codeSystemOid);

    void deleteCode(String codeSystemOid, String codeId);
}
