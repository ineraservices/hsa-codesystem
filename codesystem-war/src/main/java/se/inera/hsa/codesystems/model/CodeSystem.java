package se.inera.hsa.codesystems.model;

import javax.json.bind.JsonbBuilder;
import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.io.Serializable;

public class CodeSystem implements Serializable {
    @Schema(required = true)
    @NotBlank
    public String codeSystemId;

    @Schema(required = true)
    @NotBlank
    public String name;

    public String hsaCodeName;
    public String hsaMetaFormat;
    public String hsaMetaIdentity;
    public String hsaCodeSyntax;
    public String hsaMetaVersion;
    public String hsaCodeEntry;
    public String hsaMetaReference;
    public String hsaCodeAttribute;

    public CodeSystem() {
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
