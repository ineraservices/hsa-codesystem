package se.inera.hsa.codesystems.exceptionhandling;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.InputStream;
import java.util.Scanner;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class catches all the errors thrown by the system and maps them according to type.
 * This class devides Exceptions into two types:
 * 1. User generated Exceptions (WebApplicationException and Client)
 *      These are Exceptions that are not system errors, but expected because the user did something wrong.
 *      ex: user searches for id that is not in the database, will throw NotFoundException (404)
 *      these will not be logged
 * 2. System exceptions
 *      These exceptions are serious errors caused by the system, and are not expected to occur
 *      ex: NullPointerException, SqlServerException, etc.
 *      These will be logged and a user friendly message will be sent in the body of the response together with a unique
 *      ExceptionReference id that can be use by administrators to pair the client-error with the log-entry.
 *
 * IMPORTANT best practice error handling:
 *   -DOs
 *      - Always throw errors according to these two types.
 *          - WebApplicationException (or subclasses thereof) when the user did something wrong/unexpected.
 *          - Other types of Exceptions when the system did something wrong/unexpected.
 *      - Specify an exception in your method signature, you should also document it in your Javadoc.
 *          - public void find(String id) throws NotFoundException {
 *          - * @throws NotFoundException if nothing is found
 *      - consider throwing a WebApplicationException (e.g NotFoundException) instead of returning null
 *      - Prefer Specific Exceptions: NumberFormatException instead of RuntimeException
 *      - Throw Exceptions with descriptive messages
 *          - if you catch an exception you can wrap it with an additional informative message
 *              e.g. throw new MyException("More info.", e);
 *   -DON'Ts
 *      - never log errors (and rethrow) inside a catch
 *          - if you do, the log handler will log it again.
 *          e.g: catch (Exception e) { log.error("oops!"); throw new MyException("Oh no!")} <- NO!
 *      - NEVER catch throwable
 *          e.g: catch (Throwable t) <- NO!
 *      - Don’t Ignore Exceptions
 *          e.g catch (NumberFormatException e) { // LOL, this will never happen } <- NO!
 */
@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger log = LogManager.getLogger(ThrowableExceptionMapper.class);

    @Context
    HttpServletRequest request;

    @Override
    public Response toResponse(Throwable throwable) {
        if (throwable instanceof WebApplicationException) {
            return ((WebApplicationException) throwable).getResponse();
        } else {
            String exceptionReference = UUID.randomUUID().toString();
            String logMessage = buildErrorMessage(exceptionReference, request);

            log.error(logMessage, throwable);

            String errorMessageResponseBody = new ErrorMessageResponseBody(
                    "Internal Server Error",
                    "An internal error occurred, please try again or contact your administrator. Supply them with the exceptionReference.",
                    exceptionReference).toJson();
            return Response.status(
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                    .entity(errorMessageResponseBody)
                    .build();
        }
    }

    private String buildErrorMessage(String exceptionReference, HttpServletRequest req) {
        StringBuilder message = new StringBuilder();
        String entity = "(empty)";

        try {
            // How to cache getInputStream: http://stackoverflow.com/a/17129256/356408
            InputStream is = req.getInputStream();
            // Read an InputStream elegantly: http://stackoverflow.com/a/5445161/356408
            Scanner s = new Scanner(is, "UTF-8").useDelimiter("\\A");
            entity = s.hasNext() ? s.next() : entity;
        } catch (Exception ex) {
            // Ignore exceptions around getting the entity
        }

        message.append("Uncaught organization-api exception:\n");
        message.append("exceptionReference=").append(exceptionReference).append("\n");
        message.append("URL: ").append(getOriginalURL(req)).append("\n");
        message.append("Method: ").append(req.getMethod()).append("\n");
        message.append("Entity: ").append(entity).append("\n");

        return message.toString();
    }

    private String getOriginalURL(HttpServletRequest req) {
        // Rebuild the original request URL: http://stackoverflow.com/a/5212336/356408
        String scheme = req.getScheme();             // http
        String serverName = req.getServerName();     // hostname.com
        int serverPort = req.getServerPort();        // 80
        String contextPath = req.getContextPath();   // /mywebapp
        String servletPath = req.getServletPath();   // /servlet/MyServlet
        String pathInfo = req.getPathInfo();         // /a/b;c=123
        String queryString = req.getQueryString();   // d=789

        // Reconstruct original requesting URL
        StringBuilder url = new StringBuilder();
        url.append(scheme).append("://").append(serverName);

        if (serverPort != 80 && serverPort != 443) {
            url.append(":").append(serverPort);
        }

        url.append(contextPath).append(servletPath);

        if (pathInfo != null) {
            url.append(pathInfo);
        }

        if (queryString != null) {
            url.append("?").append(queryString);
        }

        return url.toString();
    }
}