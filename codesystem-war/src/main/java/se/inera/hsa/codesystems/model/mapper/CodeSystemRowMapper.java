package se.inera.hsa.codesystems.model.mapper;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import se.inera.hsa.codesystems.jsonb.JsonbUtil;
import se.inera.hsa.codesystems.model.CodeSystem;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeSystemRowMapper implements RowMapper<CodeSystem> {
    public CodeSystemRowMapper() {
    }

    @Override
    public CodeSystem map(ResultSet rs, StatementContext ctx) throws SQLException {
        String codesystemJson = rs.getNString("codesystem_json");
        return JsonbUtil.jsonb().fromJson(codesystemJson, CodeSystem.class);
    }
}
