package se.inera.hsa.codesystems.excelimporter;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class ExcelToSQLCreator {

    private final static String CODESYSTEMS_DATA_FILENAME = "dbfill.sql";
    private String inputExcelFileName;
    private File outputFile;

    public ExcelToSQLCreator(String inputExcelFileName, String databaseName) {
        this.inputExcelFileName = inputExcelFileName;
        outputFile = new File(CODESYSTEMS_DATA_FILENAME);

        // Clean old file
        outputFile.delete();

        // Print which file has been used to generate sql file.
        writeStringToFile("-- File used to generate this SQL:  " + inputExcelFileName + "\n\n");

        // State which database to use at beginning of file
        writeStringToFile("USE " + databaseName + ";\nGO\n");
    }

    public void createCodeSystemsSql(){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            //Create the input stream from the xlsx/xls file
            FileInputStream fis = new FileInputStream(this.inputExcelFileName);

            //Create Workbook instance for xlsx/xls file input stream
            Workbook workbook = new XSSFWorkbook(fis);

            int numberOfSheets = workbook.getNumberOfSheets();
            stringBuilder.append("INSERT INTO codesystem (codesystem_oid, codesystem_json) VALUES\n");
            for (int i = 0; i < numberOfSheets; i++) {
                Sheet sheet = workbook.getSheetAt(i);
                String name = sheet.getRow(0).getCell(1).getStringCellValue();
                String oid = sheet.getRow(1).getCell(1).getStringCellValue().trim();

                stringBuilder.append("('").append(oid).append("', '{\"codeSystemId\":\"").append(oid).append("\",\"name\":\"").append(name).append("\"}')");
                if (i == numberOfSheets-1){
                    stringBuilder.append(";\n");
                } else {
                    stringBuilder.append(",\n");
                }

            }
            stringBuilder.append("GO\n\n\n");
            fis.close();

            this.writeStringToFile(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createCodeSql(){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            //Create the input stream from the xlsx/xls file
            FileInputStream fis = new FileInputStream(this.inputExcelFileName);

            //Create Workbook instance for xlsx/xls file input stream
            Workbook workbook = new XSSFWorkbook(fis);

            int numberOfSheets = workbook.getNumberOfSheets();

            for (int i = 0; i < numberOfSheets; i++) {

                stringBuilder.append("INSERT INTO code (code_id, parent_oid, code_json) VALUES\n");
                Sheet sheet = workbook.getSheetAt(i);
                String codeSystemName = sheet.getRow(0).getCell(1).getStringCellValue();
                String codeSystemOid = sheet.getRow(1).getCell(1).getStringCellValue().trim();


                //System.out.println(codeSystemOid + " " + codeSystemName);
                Iterator<Row> rowIterator = sheet.rowIterator();
                int startrow = this.getStartRow(sheet);
                while (rowIterator.hasNext()){
                    Row row = rowIterator.next();
                    if (row.getRowNum() > startrow){
                        // System.out.println("rownum = " + row.getRowNum() + " " + this.getCellValueAsString(row.getCell(0)));
                        String parsedRow;

                        // Exceptions
                        switch (codeSystemOid) {
                            case "1.2.752.129.2.2.1.17":
                                // Kommunkod
                                parsedRow = (this.parseMunicipalityRow(row, codeSystemOid));
                                break;
                            case "1.2.752.129.2.2.1.98":
                                // Godkända system-id
                                parsedRow = (this.parseHsaSystemRole(row, codeSystemOid));
                                break;
                            case "1.2.752.29.23.1.100":
                                // Medarbetaruppdragets ändamål
                                parsedRow = (this.parseHsaCommissionPurposeRow(row, codeSystemOid));
                                break;
                            case "1.2.752.129.2.2.1.18":
                                // Länskod
                                parsedRow = (this.parseCountyRow(row, codeSystemOid));
                                break;
                            case "1.2.752.129.2.2.1.99":
                                // Reserverade funktionsnamn
                                parsedRow = (this.parseHsaSystemRole(row, codeSystemOid));
                                break;
                            default:
                                // Default
                                parsedRow = (this.parseRow(row, codeSystemOid));
                                break;
                        }

                        if ("".equals(parsedRow)) {
                            break;
                        }

                        if (isLastRow(sheet, row)) {
                            stringBuilder.append(parsedRow).append(";\nGO\n");
                            break;
                        } else {
                            stringBuilder.append(parsedRow).append(",\n");
                        }
                    }
                }

                stringBuilder.append("\n");
            }
            fis.close();

            this.writeStringToFile(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // is this the last row in the sheet
    private boolean isLastRow(Sheet sheet, Row row) {
        int rowNum = row.getRowNum();
        Row nextRow = sheet.getRow(rowNum+1);
        if(nextRow == null) {
            return true;
        }
        Cell nextCell0 = nextRow.getCell(0);

        return (nextCell0 == null || nextCell0.getCellType() == CellType.BLANK);
    }

    private String parseRow(Row row, String oid) {

        Cell cell0 = row.getCell(0);
        Cell cell1 = row.getCell(1);
        Cell cell2 = row.getCell(2);

        String lastModifiedDate = this.createLastModifiedDate();


        String code;
        String name;
        String desc;

        code = getCellValueAsString(cell0);
        name = getCellValueAsString(cell1);
        desc = getCellValueAsString(cell2);

        if ("".equals(desc)){
            desc = name;
        }

        return "('" + code + "', '" + oid + "', '{\"code\":\"" + code + "\",\"codeDescription\":\"" + desc + "\",\"lastModifiedDate\":\"" + lastModifiedDate + "\",\"name\":\"" + name + "\",\"parentOid\":\"" + oid + "\"}')";
    }

    private String parseMunicipalityRow(Row row, String oid) {

        Cell cell0 = row.getCell(0);
        Cell cell1 = row.getCell(1);
        Cell cell2 = row.getCell(2);

        String lastModifiedDate = this.createLastModifiedDate();

        String code;
        String municipalityCode;
        String countyCode;
        String name;
        String desc = "Kommun";

        municipalityCode = getCellValueAsString(cell0);
        if (municipalityCode.length() == 1){
            municipalityCode = "0" + municipalityCode;
        }

        countyCode = getCellValueAsString(cell2);
        code = countyCode + municipalityCode;

        name = getCellValueAsString(cell1);


        return "('" + code + "', '" + oid + "', '{\"code\":\"" + code + "\",\"codeDescription\":\"" + desc + "\",\"lastModifiedDate\":\"" + lastModifiedDate + "\",\"name\":\"" + name + "\",\"parentOid\":\"" + oid + "\"}')";
    }

    private String parseHsaSystemRole(Row row, String oid) {

        Cell cell0 = row.getCell(0);
        Cell cell1 = row.getCell(1);

        String lastModifiedDate = this.createLastModifiedDate();

        String code = getCellValueAsString(cell0);
        String name = code;
        String desc = getCellValueAsString(cell1);

        return "('" + code + "', '" + oid + "', '{\"code\":\"" + code + "\",\"codeDescription\":\"" + desc + "\",\"lastModifiedDate\":\"" + lastModifiedDate + "\",\"name\":\"" + name + "\",\"parentOid\":\"" + oid + "\"}')";
    }

    //
    private String parseHsaCommissionPurposeRow(Row row, String oid) {
        return this.parseHsaSystemRole(row, oid);
    }



    private String parseCountyRow(Row row, String oid) {

        Cell cell0 = row.getCell(0);
        Cell cell1 = row.getCell(1);


        String lastModifiedDate = this.createLastModifiedDate();

        String code = getCellValueAsString(cell0);
        String name = getCellValueAsString(cell1);
        String desc = "Länskod " + name;

        return "('" + code + "', '" + oid + "', '{\"code\":\"" + code + "\",\"codeDescription\":\"" + desc + "\",\"lastModifiedDate\":\"" + lastModifiedDate + "\",\"name\":\"" + name + "\",\"parentOid\":\"" + oid + "\"}')";
    }





    private String getCellValueAsString(Cell cell){
        String retval = "";
        if (cell != null) {
            CellType cellType = cell.getCellType();
            if (cellType == CellType.NUMERIC) {
                retval = (cell.getNumericCellValue() + "").replace(".0", "");
            } else if (cellType == CellType.STRING) {

                retval = cell.getStringCellValue();
                retval =  (retval.replaceAll("\n" , " "));
                retval =  (retval.replaceAll("\"" , "''"));
            } else if (cellType == CellType.BLANK) {
                retval = "";
            } else {
                retval = "Unknown cell type!";
            }
        }
        return retval.trim();
    }

    private int getStartRow(Sheet sheet){
        int retval = 5; // default
        String oid = sheet.getRow(1).getCell(1).getStringCellValue();

        if (oid.equals("1.2.752.29.23.1.102") || oid.equals("1.2.752.29.23.1.99") || oid.equals("1.2.752.29.23.1.101") || oid.equals("")) {
            retval += 1;
        }
        return retval;
    }

    private String createLastModifiedDate(){
        // 2019-09-19T16:27:05.916632Z[UTC]
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'[z]");
        return simpleDateFormat.format(new Date());
    }


    private void writeStringToFile(String data){
        try {
            FileWriter fw = new FileWriter(this.outputFile, true);
            fw.write(data);

            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
