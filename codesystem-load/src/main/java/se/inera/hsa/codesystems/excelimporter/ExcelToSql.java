package se.inera.hsa.codesystems.excelimporter;

public class ExcelToSql {

    public static void main(String[] args){

        if (args.length < 2) {
            System.out.println("Usage: App <excel-file> <db-name>");
            System.exit(0);
        } else {
            ExcelToSQLCreator creator = new ExcelToSQLCreator(args[0], args[1]);
            creator.createCodeSystemsSql();
            creator.createCodeSql();
        }

    }
}
