package se.inera.hsa.junit5.extension;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.Properties;
import java.util.logging.Logger;

public class RestAssuredExtension implements BeforeAllCallback, AfterAllCallback {
    private Properties props = new Properties();
    private static final Logger LOG = Logger.getLogger(RestAssuredExtension.class.getName());

    public RestAssuredExtension() {
        try {
            props.load(RestAssuredExtension.class.getResourceAsStream("test.properties"));
        } catch (Exception e) {
            LOG.warning("Unable to find test.properties, will try with default values.");
        }
    }

    @Override
    public void afterAll(ExtensionContext context) {
        RestAssured.reset();
    }

    @Override
    public void beforeAll(ExtensionContext context) {

        int port = Integer.parseInt(getProperty("port"));
        String baseUri = getProperty("baseUri");
        String basePath = getProperty("basePath");

        LOG.info("Running tests on " + baseUri + ":" + port + basePath);

        RestAssured.port = port;
        RestAssured.baseURI = baseUri;
        RestAssured.basePath = basePath;

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
        RestAssured.defaultParser = Parser.JSON;
    }

    private String getProperty(String propertyName) {
        return System.getProperty(propertyName, props.getProperty(propertyName));
    }
}
