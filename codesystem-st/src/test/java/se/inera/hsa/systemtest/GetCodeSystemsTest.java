package se.inera.hsa.systemtest;

import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.junit5.extension.RestAssuredExtension;

import java.time.LocalDateTime;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;
import static se.inera.hsa.systemtest.TestApi.*;

@ExtendWith(RestAssuredExtension.class)
class GetCodeSystemsTest {
    private static String codeSystemId = "999";
    private static String codeSystemName = "testkodverk";
    private static String code = "1";
    private static String codeName = "testkod";
    private static String codeExpire3month = "2";
    private static String codeNameExpire3month = "UtgårOm3månader";
    private static String codeExpiredYesterday = "3";
    private static String codeNameExpiredYesterday = "UtgickIgår";
    private static String expireDate3month = LocalDateTime.now().plusMonths(3).toString() + "Z";
    private static String expireDate3monthSP = LocalDateTime.now().plusMonths(3).toString() + "Z";
    private static String expireDateYesterday = LocalDateTime.now().minusDays(1).toString() + "Z";

    @BeforeAll
    static void initCodeSystemWithTestdata() {
        //Add new CodeSystem as testobjekt

        TestApi.deleteCodeSystem(codeSystemId);

        assertEquals(204, createCodeSystem(codeSystemId, codeSystemName));

        //Add code in the Codesystem as testattribute without expiredate
        assertEquals(204, addTestCode(codeSystemId, code, codeName));

        //Add code in the Codesystem with ExpireDate 3 month ahead in time
        assertEquals(204, addTestCode(codeSystemId, codeExpire3month, codeNameExpire3month, expireDate3month));

        //Add code in the Codesystem with ExpireDate yesterday
        assertEquals(204, addTestCode(codeSystemId, codeExpiredYesterday, codeNameExpiredYesterday, expireDateYesterday));
    }

    @Test
    @DisplayName("GETallCodeSystems")
    @Tag("codesystem")
    @Tag("GET")
    void testGETallCodeSystems() throws ParseException {
        Response systemsResponse = getCodeSystems();

        //Verify OK status code
        systemsResponse.then().statusCode(200);

        JSONParser parser = new JSONParser();
        JSONArray codeSystemsJson = (JSONArray) parser.parse(systemsResponse.getBody().asString());

        //Verify existens of 19 preloaded codeSystems + 1 extra from @BeforeAll
        assertEquals(20, codeSystemsJson.size());
    }

    @Test
    @DisplayName("GETspecificCodeSystem")
    @Tag("codesystem")
    @Tag("GET")
    void testGETspecificCodeSystem() {
        Response codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(200);
        codeSystemResponse.then().body("codeSystemId", equalTo(codeSystemId));
        codeSystemResponse.then().body("name", equalTo(codeSystemName));
    }

    @Test
    @DisplayName("GETallCodesInCodeSystem")
    @Tag("codesystem")
    @Tag("GET")
    void testGETallCodesInCodeSystem() throws ParseException {
        Response codesResponse = getCodeSystemCodes(codeSystemId);
        //Verify OK status code
        codesResponse.then().statusCode(200);

        JSONParser parser = new JSONParser();
        JSONArray codesJson = (JSONArray) parser.parse(codesResponse.getBody().asString());
        //Verify existens of valid 2 codes from @BeforeAll (1 is expired and should not be returned)
        assertEquals(2, codesJson.size());

        //Verify the code and name of the preloaded codes
        String codeJson = codesJson.toJSONString();
        assertTrue(codeJson.contains(code));
        assertTrue(codeJson.contains(codeExpire3month));

        assertTrue(codeJson.contains(codeName));
        assertTrue(codeJson.contains(codeNameExpire3month));
    }

    @Test
    @DisplayName("SetExpireDateWithWrongFormat")
    @Tag("codesystem")
    @Tag("GET")
    void testSetExpireDateWithWrongFormat() {
        //Try empty string
        String expireDate = "";
        assertEquals(500, addTestCode(codeSystemId, "testkod", "testnamn", expireDate));

        //Try other faulty format
        expireDate = "3'e maj 2021";
        assertEquals(500, addTestCode(codeSystemId, "testkod", "testnamn", expireDate));
    }

    @Test
    @DisplayName("GETspecificCodeOfCodeSystem")
    @Tag("codesystem")
    @Tag("GET")
    void testGETspecificCodeOfCodeSystem() {
        //Verify code is present.
        Response codeResponse = getCode(codeSystemId, code);
        codeResponse.then().statusCode(200);
        codeResponse.then().body("code",equalTo(code));
        codeResponse.then().body("name",equalTo(codeName));
    }

    @Test
    @DisplayName("GETCodeWithExpireDate")
    @Tag("codesystem")
    @Tag("GET")
    void testGETCodeWithExpireDate() throws ParseException {
        //Verify code is present.
        Response codeResponse = getCode(codeSystemId, codeExpire3month);
        codeResponse.then().statusCode(200);
        codeResponse.then().body("code",equalTo(codeExpire3month));
        codeResponse.then().body("name",equalTo(codeNameExpire3month));


        // Denna kod finns för att det är problem med Java 11 på windows-maskiner
        JSONParser parser = new JSONParser();
        JSONObject codesJson = (JSONObject) parser.parse(codeResponse.getBody().asString());
        String codeJsonS = codesJson.toJSONString();

        //System.out.println("assp : " + codesJson.get("expireDate"));
        String expireDate = (String)codesJson.get("expireDate");
        //System.out.println("String expire : " + expireDate);

        String temp = expireDate3month.substring(0, expireDate.length()-1) + "Z";

        codeResponse.then().body("expireDate",equalTo(temp));
    }

    private String removeLastChar(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == '0') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @Test
    @DisplayName("tryGETexpiredCode")
    @Tag("codesystem")
    @Tag("GET")
    void testtryGETexpiredCode() {
        //Verify code is not returned.
        Response codeResponse = getCode(codeSystemId, codeExpiredYesterday);
        codeResponse.then().statusCode(404);
    }

    @AfterAll
    static void removedTestdataFromCodeSystem () {
        //Delete codesystem again to clean-up
        assertEquals(204, deleteCodeSystem(codeSystemId));
    }
}
