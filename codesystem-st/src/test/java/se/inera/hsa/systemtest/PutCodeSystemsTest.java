package se.inera.hsa.systemtest;

import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.junit5.extension.RestAssuredExtension;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static se.inera.hsa.systemtest.TestApi.*;

@ExtendWith(RestAssuredExtension.class)
class PutCodeSystemsTest {

    @Test
    @DisplayName("Create and delete a new CodeSystem")
    @Tag("codesystem")
    @Tag("PUT")
    @Tag("GET")
    @Tag("DELETE")
    void PutNewCodeSystem() {
        String codeSystemId ="666";
        String name ="testkodverk";

        TestApi.deleteCodeSystem(codeSystemId);

        //Add new CodeSystem
        assertEquals(204, createCodeSystem(codeSystemId, name));

        //Verify new codesystem is present.
        Response codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(200);
        codeSystemResponse.then().body("codeSystemId",equalTo(codeSystemId));
        codeSystemResponse.then().body("name", equalTo(name));

        //Delete codesystem again to clean-up
        assertEquals(204, deleteCodeSystem(codeSystemId));

        //Verify codesystem is deleted.
        codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(404); //404 = Endpoint not existing
    }

    @Test
    @DisplayName("Create and delete a new Code")
    @Tag("codesystem")
    @Tag("PUT")
    @Tag("GET")
    @Tag("DELETE")
    void PutNewCode() {
        //First create new codesystem to use for test when adding new code
        String codeSystemId = "777";
        String codeSystemName = "testcodeSystem";
        String code = "55";
        String codeName = "testcode";

        TestApi.deleteCodeSystem(codeSystemId);

        //Add new CodeSystem
        assertEquals(204, createCodeSystem(codeSystemId, codeSystemName));

        //Verify new codesystem is present.
        Response codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(200);
        codeSystemResponse.then().body("codeSystemId",equalTo(codeSystemId));
        codeSystemResponse.then().body("name", equalTo(codeSystemName));

        //Add code in the Codesystem
        assertEquals(204, addTestCode(codeSystemId, code, codeName));

        //Verify new code is present.
        Response codeResponse = getCode(codeSystemId, code);
        codeResponse.then().statusCode(200);
        codeResponse.then().body("code",equalTo(code));
        codeResponse.then().body("name", equalTo(codeName));

        //Delete codesystem again to clean-up
        assertEquals(204, deleteCodeSystem(codeSystemId));

        //Verify that codesystem is deleted.
        codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(404); //404 = Endpoint not existing
    }

    @Test
    @DisplayName("Create a new CodeSystem, change its name and delete the codesystem again")
    @Tag("codesystem")
    @Tag("PUT")
    @Tag("GET")
    @Tag("DELETE")
    void PutNewNameCodeSystem() {
        //First create new codesystem to use for test
        String codeSystemId = "888";
        String codeSystemName = "Orginalnamn";
        String codeSystemNewName = "Bytt namn";

        TestApi.deleteCodeSystem(codeSystemId);

        //Add new CodeSystem
        assertEquals(204, createCodeSystem(codeSystemId, codeSystemName));

        //Verify new codesystem is present.
        Response codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(200);
        codeSystemResponse.then().body("codeSystemId",equalTo(codeSystemId));
        codeSystemResponse.then().body("name", equalTo(codeSystemName));

        //Update CodeSystem with new name
        assertEquals(204, createCodeSystem(codeSystemId, codeSystemNewName));

        //Verify codesystem is present with new name.
        codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(200);
        codeSystemResponse.then().body("codeSystemId",equalTo(codeSystemId));
        codeSystemResponse.then().body("name", equalTo(codeSystemNewName));

        //Delete codesystem again to clean-up
        assertEquals(204, deleteCodeSystem(codeSystemId));

        //Verify codesystem is deleted.
        codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(404); //404 = Endpoint not existing
    }

    @Test
    @DisplayName("Change the name of a code")
    @Tag("codesystem")
    @Tag("PUT")
    @Tag("GET")
    @Tag("DELETE")
    void PutNewCodeName() {
        //First create new codesystem to use for test when adding new code
        String codeSystemId = "999";
        String codeSystemName = "testcodeSystem";
        String code = "66";
        String codeName = "testcode";
        String newCodeName = "Ny testcode";

        TestApi.deleteCodeSystem(codeSystemId);

        //Add new CodeSystem
        assertEquals(204, createCodeSystem(codeSystemId, codeSystemName));

        //Verify codesystem is present.
        Response codeSystemResponse = getCodeSystem(codeSystemId);
        codeSystemResponse.then().statusCode(200);
        codeSystemResponse.then().body("codeSystemId",equalTo(codeSystemId));
        codeSystemResponse.then().body("name", equalTo(codeSystemName));

        //Add code in the Codesystem
        assertEquals(204, addTestCode(codeSystemId, code, codeName));

        //Verify code is present.
        Response codeResponse = getCode(codeSystemId, code);
        codeResponse.then().statusCode(200);
        codeResponse.then().body("code",equalTo(code));
        codeResponse.then().body("name", equalTo(codeName));

        //Update code with new name
        assertEquals(204, putNewCodeName(codeSystemId, code, newCodeName));

        //Verify code is present.
        codeResponse = getCode(codeSystemId, code);
        codeResponse.then().statusCode(200);
        codeResponse.then().body("code",equalTo(code));
        codeResponse.then().body("name", equalTo(newCodeName));

        //Delete codesystem again to clean-up
        assertEquals(204, deleteCodeSystem(codeSystemId));

        //Verify codesystem is deleted.
        codeResponse = getCodeSystem(codeSystemId);
        codeResponse.then().statusCode(404);
    }
}
