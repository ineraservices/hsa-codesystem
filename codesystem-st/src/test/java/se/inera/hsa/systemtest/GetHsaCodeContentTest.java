package se.inera.hsa.systemtest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.junit5.extension.RestAssuredExtension;

import static org.hamcrest.Matchers.equalTo;
import static se.inera.hsa.systemtest.TestApi.getCode;

@ExtendWith(RestAssuredExtension.class)
@DisplayName("Test code samples in all preloaded codesystems in HSA")
class GetHsaCodeContentTest {
    private static String befattningskod_Oid = "1.2.752.129.2.2.1.4";
    private static String chefskod_Oid = "1.2.752.29.23.1.103";
    private static String enhetstyp_Oid = "1.2.752.129.2.2.1.12";
    private static String finansierandeLandstingKommun_Oid = "1.2.752.129.2.2.1.97";
    private static String kummunkod_Oid = "1.2.752.129.2.2.1.17";
    private static String legitimeradYrkesgrupp_Oid = "1.2.752.29.23.1.6";
    private static String lanskod_Oid = "1.2.752.129.2.2.1.18";
    private static String medarbetaruppdragetsRattigheter_aktivitet_Oid = "1.2.752.29.23.1.102";
    private static String medarbetaruppdragetsRattigheter_informationstyp_Oid = "1.2.752.29.23.1.99";
    private static String medarbetaruppdragetsRattigheter_informationsomfang_Oid = "1.2.752.29.23.1.101";
    private static String utokadYrkeskod_Oid = "1.2.752.129.2.2.1.96";
    private static String verksamhetskod_Oid = "1.2.752.129.2.2.1.3";
    private static String visasFor_Oid = "1.2.752.29.23.1.11";
    private static String vardOchOmsorgsform_Oid = "1.2.752.129.2.2.1.13";
    private static String gruppforskrivarkod_Oid = "1.2.752.129.2.2.1.95";

    //Nedan kodverk är i praktiken listor med giltiga strängar. Kod=Klartext i dessa fall
    private static String agarform_Oid = "1.2.752.129.2.2.1.14";
    private static String medarbetaruppdragets_andamal_Oid = "1.2.752.29.23.1.100";
    private static String godkandaSystem_id_Oid = "1.2.752.129.2.2.1.98";
    private static String reserveradeFunktionsnamn_Oid = "1.2.752.129.2.2.1.99";

    @Test
    @DisplayName("Test code sample in \"Befattningskod\"=1.2.752.129.2.2.1.4")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificPAtitleCode() {
        String paTitleCode = "659090";
        String paTitleName = "Städ-, tvätt- och renhållningsarbete, annat";

        //Verify code is present.
        getCode(befattningskod_Oid, paTitleCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(paTitleCode))
            .body("name",equalTo(paTitleName))
            .body("parentOid",equalTo(befattningskod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Chefskod\"=1.2.752.29.23.1.103")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificManagerCode() {
        String managerCode = "L";
        String managerName = "Arbetstagare med lednings- och samordningsansvar med begränsat ansvar, det vill säga ansvarar för en eller två av områdena verksamhet, ekonomi och personal.";

        //Verify code is present.
        getCode(chefskod_Oid, managerCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(managerCode))
            .body("name",equalTo(managerName))
            .body("parentOid",equalTo(chefskod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Enhetstyp\"=1.2.752.129.2.2.1.12")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificBusinessTypeCode() {
        String businessTypeCode = "03";
        String businessTypeName = "distansenhet";

        //Verify code is present.
        getCode(enhetstyp_Oid, businessTypeCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(businessTypeCode))
            .body("name",equalTo(businessTypeName))
            .body("parentOid",equalTo(enhetstyp_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Finansierande Landsting-Kommun\"=1.2.752.129.2.2.1.97")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificFinancingOrganizationCode() {
        String financingOrganizationCode = "212000-1850";
        String financingOrganizationName = "Karlstads kommun";

        //Verify code is present.
        getCode(finansierandeLandstingKommun_Oid, financingOrganizationCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(financingOrganizationCode))
            .body("name",equalTo(financingOrganizationName))
            .body("parentOid",equalTo(finansierandeLandstingKommun_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Kommunkod\"=1.2.752.129.2.2.1.17")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificMunicipalityCode() {
        String countyCode = "17";
        String municipalityCode = "80";
        String municipalityName = "Karlstad";

        //Verify code is present.
        getCode(kummunkod_Oid, countyCode + municipalityCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(countyCode + municipalityCode))
            .body("name",equalTo(municipalityName))
            .body("parentOid",equalTo(kummunkod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Legitimerad yrkesgrupp\"=1.2.752.29.23.1.6")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificTitleCode() {
        String titleCode = "NA";
        String titleName = "Naprapat";

        //Verify code is present.
        getCode(legitimeradYrkesgrupp_Oid, titleCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(titleCode))
            .body("name",equalTo(titleName))
            .body("parentOid",equalTo(legitimeradYrkesgrupp_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Länskod\"=1.2.752.129.2.2.1.18")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificCountyCode() {
        String countyCode = "14";
        String countyName = "Västra Götalands län";

        //Verify code is present.
        getCode(lanskod_Oid, countyCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(countyCode))
            .body("name",equalTo(countyName))
            .body("parentOid",equalTo(lanskod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Medarbetaruppdragets rättigheter aktivitet\"=1.2.752.29.23.1.102")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificCommissionRight_ActivityCode() {
        String activityCode = "Läsa";
        String activityName = "Läsa";

        //Verify code is present.
        getCode(medarbetaruppdragetsRattigheter_aktivitet_Oid, activityCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(activityCode))
            .body("name",equalTo(activityName))
            .body("parentOid",equalTo(medarbetaruppdragetsRattigheter_aktivitet_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Medarbetaruppdragets rättigheter informationstyp\"=1.2.752.29.23.1.99")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificCommissionRight_InfoTypeCode() {
        String infoTypeCode = "vpo";
        String infoTypeName = "Vård- och omsorgsplan ostrukt";

        //Verify code is present.
        getCode(medarbetaruppdragetsRattigheter_informationstyp_Oid, infoTypeCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(infoTypeCode))
            .body("name",equalTo(infoTypeName))
            .body("parentOid",equalTo(medarbetaruppdragetsRattigheter_informationstyp_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Medarbetaruppdragets rättigheter informationsomfång\"=1.2.752.29.23.1.101")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificCommissionRight_OrganizationScopeCode() {
        String orgScopeCode = "SJF";
        String orgScopeName = "Sammanhållen journalföring";

        //Verify code is present.
        getCode(medarbetaruppdragetsRattigheter_informationsomfang_Oid, orgScopeCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(orgScopeCode))
            .body("name",equalTo(orgScopeName))
            .body("parentOid",equalTo(medarbetaruppdragetsRattigheter_informationsomfang_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Utökad yrkeskod\"=1.2.752.129.2.2.1.96")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificOccupationalCode() {
        String occupationalCode = "AD";
        String occupationalName = "Administratör av dospatientuppgifter";

        //Verify code is present.
        getCode(utokadYrkeskod_Oid, occupationalCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(occupationalCode))
            .body("name",equalTo(occupationalName))
            .body("parentOid",equalTo(utokadYrkeskod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Verksamhetskod\"=1.2.752.129.2.2.1.3")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificBusinessClassificationCode() {
        String businessClassificationCode = "1801";
        String businessClassificationName = "Akutverksamhet, allmäntandvård";

        //Verify code is present.
        getCode(verksamhetskod_Oid, businessClassificationCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(businessClassificationCode))
            .body("name",equalTo(businessClassificationName))
            .body("parentOid",equalTo(verksamhetskod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Visas för\"=1.2.752.29.23.1.11")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificDestinationIndicatorCode() {
        String destinationIndicatorCode = "04";
        String destinationIndicatorName = "Fri användning av vem som helst (s.k. öppen data)";

        //Verify code is present.
        getCode(visasFor_Oid, destinationIndicatorCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(destinationIndicatorCode))
            .body("name",equalTo(destinationIndicatorName))
            .body("parentOid",equalTo(visasFor_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Vård och Omsorgsform\"=1.2.752.129.2.2.1.13")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificCareTypeCode() {
        String careTypeCode = "04";
        String careTypeName = "Socialtjänst";

        //Verify code is present.
        getCode(vardOchOmsorgsform_Oid, careTypeCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(careTypeCode))
            .body("name",equalTo(careTypeName))
            .body("parentOid",equalTo(vardOchOmsorgsform_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Gruppförskrivarkod\"=1.2.752.129.2.2.1.95")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificGroupPrescriptionCode() {
        String groupPrescriptionCode = "9600008";
        String groupPrescriptionName = "Sjuksköterska (diabetes)";

        //Verify code is present.
        getCode(gruppforskrivarkod_Oid, groupPrescriptionCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(groupPrescriptionCode))
            .body("name",equalTo(groupPrescriptionName))
            .body("parentOid",equalTo(gruppforskrivarkod_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Medarbetaruppdragets ändamål\"=1.2.752.29.23.1.100")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificCommissionPurposeCode() {
        String purposeCode = "Administration";
        String purposeName = "Administration";

        //Verify code is present.
        getCode(medarbetaruppdragets_andamal_Oid, purposeCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(purposeCode))
            .body("name",equalTo(purposeName))
            .body("parentOid",equalTo(medarbetaruppdragets_andamal_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Ägarform\"=1.2.752.129.2.2.1.14")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificManagmentCode() {
        String managementCode = "Privat";
        String managementName = "Privat";

        //Verify code is present.
        getCode(agarform_Oid, managementCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(managementCode))
            .body("name",equalTo(managementName))
            .body("parentOid",equalTo(agarform_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Godkända System-id\"=1.2.752.129.2.2.1.98")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificSystemRoleCode() {
        String systemRoleCode = "RJH";
        String systemRoleName = "RJH";

        //Verify code is present.
        getCode(godkandaSystem_id_Oid, systemRoleCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(systemRoleCode))
            .body("name",equalTo(systemRoleName))
            .body("parentOid",equalTo(godkandaSystem_id_Oid));
    }

    @Test
    @DisplayName("Test code sample in \"Reserverade Funktionsnamn\"=1.2.752.129.2.2.1.99")
    @Tag("codesystem")
    @Tag("DATA")
    void GETspecificFunctionNameCode() {
        String functionNameCode = "Operationskoordinator";
        String functionNameName = "Operationskoordinator";

        //Verify code is present.
        getCode(reserveradeFunktionsnamn_Oid, functionNameCode)
        .then()
            .statusCode(200)
            .body("code",equalTo(functionNameCode))
            .body("name",equalTo(functionNameName))
            .body("parentOid",equalTo(reserveradeFunktionsnamn_Oid));
    }
}
