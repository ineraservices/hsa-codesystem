package se.inera.hsa.systemtest;

import io.restassured.response.Response;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

class TestApi {
    private static String apiversion = "codesystems";

    static Response getCodeSystems() {
        return given()
                .get("/" + apiversion);
    }

    static Response getCodeSystem(String codeSystemId) {
        return given()
            .get("/" + apiversion + "/" + codeSystemId);
    }

    static Response getCodeSystemCodes(String codeSystemId) {
        return given()
                .get("/" + apiversion + "/" + codeSystemId + "/codes");
    }

    static Response getCode(String codeSystemId, String code) {
        return given()
            .get("/" + apiversion + "/" + codeSystemId + "/codes/" + code);
    }
    
    @SuppressWarnings("unchecked")
    static Integer createCodeSystem(String codeSystemId, String name) {
        //Prepare Json for the request body of PUT
        JSONObject payload = new JSONObject();
        payload.put("codeSystemId", codeSystemId);
        payload.put("name", name);

        return given()
            .body(payload.toJSONString())
        .put("/" + apiversion + "/")  ///save Codesystem
            .statusCode();
    }

    static Integer deleteCode(String codeSystemId, String code) {
        //Prepare Json for the request body of PUT
        JSONObject payload = new JSONObject();
        payload.put("code", code);

        return given()
                .body(payload.toJSONString())
                .delete("/" + apiversion + "/" + codeSystemId + "/codes/" + code)
                .statusCode();
    }

    @SuppressWarnings("unchecked")
    static Integer addTestCode(String codeSystemId, String code, String codeName) {
        //Add new code in the codesystem
        //Prepare Json for the request body of PUT
        JSONObject payload = new JSONObject();
        payload.put("code", code);
        payload.put("name", codeName);
        payload.put("parentOid", codeSystemId);
        payload.put("codeDescription", "Detta är en testkod som läggs till i testsyfte");
        payload.put("extra1", "extra1");
        payload.put("extra2", "extra2");
        payload.put("extra3", "extra3");

        return given()
            .body(payload.toJSONString())
        .put("/" + apiversion + "/" + codeSystemId + "/codes")
            .statusCode();
    }

    @SuppressWarnings("unchecked")
    static Integer addTestCode(String codeSystemId, String code, String codeName, String inExpireDate) {
        //Add new code in the codesystem
        //Prepare Json for the request body of PUT
        JSONObject payload = new JSONObject();
        payload.put("code", code);
        payload.put("name", codeName);
        payload.put("parentOid", codeSystemId);
        payload.put("codeDescription", "Detta är en testkod som läggs till i testsyfte");
        payload.put("expireDate", inExpireDate);
        payload.put("extra1", "extra1");
        payload.put("extra2", "extra2");
        payload.put("extra3", "extra3");

        return given()
                .body(payload.toJSONString())
                .put("/" + apiversion + "/" + codeSystemId + "/codes")
                .statusCode();
    }

    @SuppressWarnings("unchecked")
    static Integer putNewCodeName(String codeSystemId, String code, String codeName) {
        Response codeResponse = getCode(codeSystemId, code);

        //Prepare Json for the request body of PUT
        JSONObject payload = new JSONObject();
        payload.put("code", code);
        payload.put("name", codeName);
        payload.put("parentOid", codeResponse.jsonPath().get("parentOid").toString());
        payload.put("codeDescription", codeResponse.jsonPath().get("codeDescription").toString());
        payload.put("extra1", codeResponse.jsonPath().get("extra1").toString());
        payload.put("extra2", codeResponse.jsonPath().get("extra2").toString());
        payload.put("extra3", codeResponse.jsonPath().get("extra3").toString());

        return given()
            .body(payload.toJSONString())
        .put("/" + apiversion + "/" + codeSystemId + "/codes")
            .statusCode();
    }

    static Integer deleteCodeSystem(String codeSystemId) {
        return given()
            .delete("/" + apiversion + "/" + codeSystemId)
            .statusCode();
    }
}
