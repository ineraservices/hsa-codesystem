package se.inera.hsa.codesystem.api;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.codesystems.model.CodeSystem;
import se.inera.hsa.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.given;

@ExtendWith({RestAssuredExtension.class})
public class CodeSystemResourceIT {

    @Test
    void whenAddCodesystems_thenGetCodeSystems() {
        String oid = "11.22.33.44.55";
    }

    @Test
    @DisplayName("Verify that codesystems and codes can be added.")
    void whenAddCodestoCodeSystem_thenGetCodes() {
        String oid = "55.44.33.22.11";
        String name = "codesystem name";

        // Add
 /*        CodeSystem cs = new CodeSystem(oid, name);
        cs.name = name;
        cs.codeSystemId = oid;
       given().log().everything().body(cs)
               .when()
               .put(RestAssured.rootPath)
               .then()
               .statusCode(204);

        List<Code> expected = new ArrayList<>();
        Code code = new Code();
        code.code = "code1";
        code.name = "name 1";
        code.codeDescription = "desc";
        code.lastModifiedDate = ZonedDateTime.now();

        expected.add(code);

        given().log().everything().body(code)
               .when()
               .put(RestAssured.rootPath + "/" + oid + "/codes")
               .then()
               .statusCode(204);

        code.code = "code2";
        code.name = "name 2";
        code.codeDescription = "desc2";
        code.lastModifiedDate = ZonedDateTime.now();
        expected.add(code);

        given().log().everything().body(code)
               .when()
               .put(RestAssured.rootPath + "/" + oid + "/codes")
               .then()
               .statusCode(204);

        List<Code> response = Arrays.asList(given().when().log().everything()
                                                   .get(RestAssured.rootPath + "/" + oid + "/codes")
                                                   .as(Code[].class)
        );
        assertThat(response.size()).isEqualTo(2);
*/
    }

    @Test
    @DisplayName("Verify that a nonexistent codesystem returns 404")
    public void getNonExistent() {
        given().when()
               .get("nonexistent")
               .then()
               .statusCode(404);
    }
}
